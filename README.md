
#info
vanilla javascript starter using webpack module bundler

##included tools
- webpack, webpack-cli, webpack-dev-center [__module bundler__]
	- webpack plugins: [html-webpack-plugin, clean-webpack-plugin]
- babel-core, babel-loader, babel-preset-env, babel-preset-react, babel-preset-stage-2 [__compiler__]

##install
- npm install

##run
- npm start

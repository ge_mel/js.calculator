module.exports = function(calculator){
	return {
			name: 'DecimalPoint',
			model: ['.'],
			view(model){
				const decimalPointHTML =`<button type="button" name="${this.name}" style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;background-color:#7c7978;color:#FFFFFF;" value="${model}" onclick="this.controller">${model}</button>`;
				return `${decimalPointHTML}`;
			},
			controller(){

				console.log(`log from ${this.name} controller`);
				// Update InputScreen with regards the point button pressed
				calculator.appElement.children['calculator'].elements.InputScreen.value += this.value;

				let inputArray = calculator.appElement.children['calculator'].elements.InputScreen.value.split('');

				/*
				check if "." already exists, like for exmpl 345+345.543. OR 345.345. OR ..345 OR .345.
					*/
				if(inputArray.length > 1 && inputArray.join('').match(new RegExp(/([+\-\*/])\w+([\.])\w+([\.])/g)) || inputArray.length > 1 && inputArray.join('').match(new RegExp(/\.\./g)) || inputArray.length > 1 && inputArray.join('').match(new RegExp(/([\.])\w+([\.])/g))) {
					inputArray.pop();
					console.log(inputArray);
					calculator.appElement.children['calculator'].elements.InputScreen.value = inputArray.join('');
					alert('Please apply math logic');
				}
			}
		};
}
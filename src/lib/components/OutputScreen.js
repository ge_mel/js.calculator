module.exports = function(calculator) {
	return {
		name: 'OutputScreen',
		model: [],
		view(){

			const outputScreenHTML = `<input name="${this.name}" style="-webkit-appearance:none;width:99.3%;height:0vh;display:inherit;font-size:2em;border:0px solid;padding:0px;margin:0px;border-color:transparent;font-weight:bold;background-color:rgb(68,228,230);color:rgb(15,134,136);" disabled>`;
			return outputScreenHTML;
			
		},
		controller(){
			console.log(`log from ${this.name} controller`);
		},
		show() {

			calculator.appElement.children['calculator'].elements[this.name].style.height = '4vh';
			calculator.appElement.children['calculator'].elements[this.name].style.border = '2px solid';
			calculator.appElement.children['calculator'].elements[this.name].style.borderColor = 'darkturquoise';
			calculator.appElement.children['calculator'].elements[this.name].style.marginBottom = '10px';
			calculator.appElement.children['calculator'].elements[this.name].style.visibility = 'visible';
		},
		hide() {
			calculator.appElement.children['calculator'].elements[this.name].style.height = '0vh';
			calculator.appElement.children['calculator'].elements[this.name].style.border = '0px solid';
			calculator.appElement.children['calculator'].elements[this.name].style.borderColor = 'transparent';
			calculator.appElement.children['calculator'].elements[this.name].style.margin = '0px';
			calculator.appElement.children['calculator'].elements[this.name].style.padding = '0px';
			calculator.appElement.children['calculator'].elements[this.name].style.visibility = 'hidden';
		}
	};
}
module.exports = function(calculator) {
	return {
			name: 'Compute',
			model: ['='],
			view(model) {

				const computeHTML = `<button type="button" style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;background-color:#8bd6be;color:#2ba076;" name="${this.name}" onclick="this.controller">${this.model}</button>`;
				return `${computeHTML}`;

			},
			controller(computeAction) { // where computeAction : true, false (calculation result button '=' pressed)

				console.log(`log from ${this.name} controller, computeAction pressed: ${computeAction}`);

				let inputArray = calculator.appElement.children['calculator'].elements.InputScreen.value.split('');

				calculator.calculatorState.InputArray = inputArray;

				calculator.appComponentsByName['OutputScreen'].show();

				/*
				check if one from last pressed button was Calulation too, 
				if yes then replace it with the latest pressed calculation
					*/
				if(inputArray.length > 1 && inputArray[inputArray.length - 1].match(new RegExp(/[+\-*/]/))) {
					
					// Calculate without last digit in inputScreen
					calculator.appElement.children['calculator'].elements.OutputScreen.value = eval(inputArray.filter((digit, i) => i !== inputArray.length - 1).join('')).toLocaleString('el-EL');; //toLocaleString(): Apply numbering formats, thousands and decimal in result

				} else if(inputArray.length) {

					try {

						/*  Octal literal (0 number prefix) error check
							regexp: ([-+/])(0)\w+ , for example 435+...-0435...*435
							*/
						if(calculator.appElement.children['calculator'].elements.InputScreen.value.match(new RegExp(/([-+/*])(0)\w+/g))) {

							calculator.appElement.children['calculator'].elements.InputScreen.value.match(new RegExp(/([-+/*])(0)\w+/g)).forEach((pattern, i) => {
								// Update InputScreen
								calculator.appElement.children['calculator'].elements.InputScreen.value = calculator.appElement.children['calculator'].elements.InputScreen.value.replace( new RegExp(/([-+/*])(0)\w+/g) , pattern.replace('0', '') );

							});

							
							if (computeAction === false) {
								// Update OutputScreen
								calculator.appElement.children['calculator'].elements.OutputScreen.value = eval(calculator.appElement.children['calculator'].elements.InputScreen.value).toLocaleString('el-EL');;

							} else {

								// Update InputScreen, OutputScreen
							 	calculator.appElement.children['calculator'].elements.InputScreen.value = eval(calculator.appElement.children['calculator'].elements.InputScreen.value); //.toLocaleString('el-EL');;
								calculator.appElement.children['calculator'].elements.OutputScreen.value = '';

								calculator.appComponentsByName['OutputScreen'].hide();

							}

						} else {
							
							if (computeAction === false) {

								// Update OutputScreen
								calculator.appElement.children['calculator'].elements.OutputScreen.value = eval(calculator.appElement.children['calculator'].elements.InputScreen.value).toLocaleString('el-EL');

							} else {

								// Update InputScreen, OutputScreen
							 	calculator.appElement.children['calculator'].elements.InputScreen.value = eval(calculator.appElement.children['calculator'].elements.InputScreen.value);
								calculator.appElement.children['calculator'].elements.OutputScreen.value = '';

								calculator.appComponentsByName['OutputScreen'].hide();

							}

						}

						
					} catch (e) {
						if(e instanceof SyntaxError) {
							console.log('Error, message:');
							console.dir(e);
						}
						
					}					

				} else {
					//...
					calculator.appComponentsByName['OutputScreen'].hide();
				}
			
			}
		};
}
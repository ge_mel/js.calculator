module.exports = function(calculator, parent) {
	return {
		name: 'Del',
		model: [],
		view() {
			const delHTML = `<button type="button" style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;background-color:#9e7070;color:#dc6868;" name="${this.name}" onclick="${this.controller}">${this.name}</button>`;
			return `${delHTML}`;
		},
		controller() {
			console.log(`log from ${this.name} controller`);
			// Update InputScreen with regards the Del(last digit) pressed

			let inputArray = calculator.appElement.children['calculator'].elements.InputScreen.value.split('');

			/*
				Delete last digit in calculation input
				*/
			if(inputArray.length > 0) {

				inputArray.pop();
				calculator.appElement.children['calculator'].elements.InputScreen.value = inputArray.join('');

			}

			// Calculate 
			// false here means that 'calculate action' is not genuinely from compute button '='
			calculator.appComponentsByName['Compute'].controller(false);

		}
	};
}
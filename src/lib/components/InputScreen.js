module.exports = function() {
	return {
		name: 'InputScreen',
		model: {},
		view() {
			const InputScreenHTML = `<input name="${this.name}" style="-webkit-appearance:none;width:100%;height:10vh;min-height:50px;border:none;font-size:3em;margin-bottom:10px;border-color:transparent;" disabled>`;
			return InputScreenHTML;
		},
		controller() {
			console.log(`log from ${this.name} controller`);
		}
	};
}
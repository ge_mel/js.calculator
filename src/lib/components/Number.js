module.exports = function (calculator){
	return {
		name: 'Number',
		model: [7,8,9,4,5,6,1,2,3,0],
		view(model) {

			const numbersHTML = model.reduce((html, number) => html + `<button type="button" name="${this.name}" style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;color:#7c7978;" value="${number}" onclick="this.controller">${number}</button>`, '');

			return `${numbersHTML}`;

		},
		controller() {
			console.log(`log from ${this.name} controller`);
			
			// Update InputScreen with regards the Number pressed
			calculator.appElement.children['calculator'].elements.InputScreen.value += parseInt(this.value);

			let inputArray = calculator.appElement.children['calculator'].elements.InputScreen.value.split('');

			/*
			check if input for some reason contains letter characters, commas or spaces
				*/
			if(inputArray.filter(char => char.match(new RegExp(/[a-z ]/gi))).length) {
				inputArray = inputArray.filter(char => !char.match(new RegExp(/[a-z ]/gi))); 
				parent.elements.InputScreen.value = inputArray.join('');
			}

			// Calculate 
			// false here means that 'calculate action' is not genuinely from compute button '='
			calculator.appComponentsByName['Compute'].controller(false);

		}
	};
}
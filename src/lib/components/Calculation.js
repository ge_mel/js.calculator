module.exports = function(calculator, parent){
	return {
		name: 'Calculation',
		model: [ '+' , '-' , '*' , '/' ],
		view(model) {

			const calculationsHTML = model.reduce((html, calculation) => html + `<button type="button" name="${this.name}" style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;background-color:#7c7978;color:#FFFFFF;" value="${calculation}" onclick="this.controller">${calculation}</button>`, '');
			return `${calculationsHTML}`;

		},
		controller() {

			console.log(`log from ${this.name} controller`);

			// Update InputScreen with regards the calculation pressed
			calculator.appElement.children['calculator'].elements.InputScreen.value += this.value;

			let inputArray = calculator.appElement.children['calculator'].elements.InputScreen.value.split('');

			/*
			check if input contains letter characters, commas or spaces
				*/
			if(inputArray.filter(char => char.match(new RegExp(/[a-z ]/gi))).length) {
				inputArray = inputArray.filter(char => !char.match(new RegExp(/[a-z ]/gi)));
				calculator.appElement.children['calculator'].elements.InputScreen.value = inputArray.join('');
			}

			/*
			check if first pressed button is *, /
				*/
			if(inputArray[0].match(new RegExp(/[*/]/g))) {
				inputArray.shift();
				calculator.appElement.children['calculator'].elements.InputScreen.value = inputArray.join('');
				alert('Please start with a number, + or -');
			}

			/*
			check if one from last pressed button was Calulation too, 
			if yes then replace it with the latest pressed calculation
				*/
			if(inputArray.length > 2 && inputArray[inputArray.length - 2].match(new RegExp(/[+\-*/]/g))) {
				inputArray[inputArray.length - 2] = inputArray[inputArray.length - 1];
				inputArray.pop();
				calculator.appElement.children['calculator'].elements.InputScreen.value = inputArray.join('');
			}

			// Calculate 
			// false here means that 'calculate action' is not genuinely from compute button '='
			calculator.appComponentsByName['Compute'].controller(false);
		}
	};
}
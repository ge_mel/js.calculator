module.exports = function (calculator){
	return {
		name: 'Reset',
		model: [],
		view() {
			const resetHTML = `<button style="outline-color:#8bd6be;-webkit-appearance:none;width:33.3%;height:10vh;min-height:50px;border:none;font-size:2em;float:left;background-color:#e25353;color:#ad2b2b;" type="reset" name="${this.name}">${this.name}</button>`;
			return `${resetHTML}`;
		},
		controller() {
			console.log(`log from ${this.name} controller`);
			// Update OutputScreen with regards the reset pressed
			calculator.appComponentsByName['OutputScreen'].hide();
		}
	};
}
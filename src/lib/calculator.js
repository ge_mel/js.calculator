export default class Calculator {
	constructor(selector){
		this.appElement = document.querySelector(selector);	// 1st Calculator property
		this.appComponentsByName = {};	// 2nd Caclulator property [app component's children]
		this.calculatorState = {
			InputArray: [],
			ComputeAction: false //  on '=' press turns true
		}
		//...
		
		this.appElement.innerHTML = this.componentTemplate();
		
		console.log('log from calculator constructor, initiated');

	}
	componentTemplate()  {
		return `<form name="calculator">
					
				</form>`;
	}
	addComponent(component){
		this.appComponentsByName[component.name] = component;
		this.appElement.children[0].innerHTML += component.view(component.model);
		console.log('log from caclulator.addComponent, component added:'+component.name);
		this.showComponent(component.name);
	}
	showComponent(name){
		this.currentComponent = this.appComponentsByName[name];
		this.updateView();
	}
	updateView(){
		if(this.currentComponent) {
			console.log('log from caclulator.updateView, for component:'+this.currentComponent.name);
			// this.appElement.children[0].innerHTML += this.currentComponent.view(this.currentComponent.model);
		}
	}
}


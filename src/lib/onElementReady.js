/* 
 * onElementReady.js
 * source: https://stackoverflow.com/questions/16149431/make-function-wait-until-element-exists
*/
const onElementReady = $element => (
  new Promise((resolve) => {
    const waitForElement = () => {
      if ($element) {
        resolve($element);
      } else {
        window.requestAnimationFrame(waitForElement);
      }
    };
    waitForElement();
  })
);

export default onElementReady;
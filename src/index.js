'use strict'

import onElementReady from './lib/onElementReady'
import Calculator from './lib/calculator'
import InputScreen from './lib/components/InputScreen'
import OutputScreen from './lib/components/OutputScreen'
import Calculation from './lib/components/Calculation'
import DecimalPoint from './lib/components/DecimalPoint'
import Delete from './lib/components/Delete'
import Number from './lib/components/Number'
import Compute from './lib/components/Compute'
import Reset from './lib/components/Reset'

const app = document.createElement('section');
app.setAttribute('id', 'app');

document.body.appendChild(app);

onElementReady(app)
	.then(() => {

		// Create calculator
		const calculator = new Calculator('#app');

		// Add InputScreen
		calculator.addComponent( new InputScreen(calculator) );

		// Add preview output screen
		calculator.addComponent( new OutputScreen(calculator) );

		// Add calculation keys
		calculator.addComponent( new Calculation(calculator) );
		onElementReady(calculator.appElement.children['calculator'].elements.Calculation)
			.then(() => {

				calculator.appElement.children['calculator'].elements.Calculation.forEach((calculation, i) => {
					calculation.addEventListener('click', calculator.appComponentsByName['Calculation'].controller), true;	
				});

			});


		// Add decimal point
		calculator.addComponent(new DecimalPoint(calculator));
		onElementReady(calculator.appElement.children['calculator'].elements.DecimalPoint)
			.then(() => {
				calculator.appElement.children['calculator'].elements.DecimalPoint.addEventListener('click', calculator.appComponentsByName['DecimalPoint'].controller, true);
			});

		// Add Delete last key (correct InputScreen.value action)
		calculator.addComponent(new Delete(calculator));
		onElementReady(calculator.appElement.children['calculator'].elements.Del)
			.then(() => {
				calculator.appElement.children['calculator'].elements.Del.addEventListener('click', calculator.appComponentsByName['Del'].controller, true);
			});
		
		// Add number keys
		calculator.addComponent(new Number(calculator));
		onElementReady(calculator.appElement.children['calculator'].elements.Number)
			.then(() => {
				
				calculator.appElement.children['calculator'].elements.Number.forEach((number, i) => {
					number.addEventListener('click', calculator.appComponentsByName['Number'].controller, true);
				});

			});

		// Add compute key
		calculator.addComponent(new Compute(calculator));
		onElementReady(calculator.appElement.children['calculator'].elements.Compute)
			.then(() => {
				calculator.appElement.children['calculator'].elements.Compute.addEventListener('click', function() {
					calculator.appComponentsByName['Compute'].controller(true);
				}, true);
			});

		// Add Reset key
		calculator.addComponent(new Reset(calculator));
		onElementReady(calculator.appElement.children['calculator'].elements.Reset)
			.then(() => {
				calculator.appElement.children['calculator'].elements.Reset.addEventListener('click', calculator.appComponentsByName['Reset'].controller, true);
			});



	}); // onElementReady.then - end